using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Boulette : MonoBehaviour
{
    public Rigidbody Boule;
    public Text nb;
    public Text time;
    public GameObject canvas;
    public Canvas won;
    public GameObject otherGameObject;
    public bool click;
    public float secondes;

        //Vitesse à assigner via l'inspecteur
    public float        speed;
    public float        jump;
    public int          nb_cube;

 

    // Start is called before the first frame update
    void Start() {
        secondes = 0;
        time.text = ((int)secondes).ToString();
        click = false; 
        won = canvas.GetComponent<Canvas>();
        won.enabled = false;
        nb_cube = 0;
        Button btn = otherGameObject.GetComponent<Button>();
	    btn.onClick.AddListener(TaskOnClick);
        Boule = GetComponent<Rigidbody>();
        jump = 10;
        speed = 2;
    }

    // Update is called once per frame
    void Update() {
        if(click) {
            secondes += Time.deltaTime;
            time.text = ((int)secondes).ToString() ;
            if ( Input.GetKey ( KeyCode.UpArrow ) ) {
                Boule.AddForce ( Vector3.forward * speed );
            }
            if ( Input.GetKey ( KeyCode.DownArrow ) ) {
                Boule.AddForce ( Vector3.back * speed );
            }
            if ( Input.GetKey ( KeyCode.LeftArrow ) ) {
                Boule.AddForce ( Vector3.left * speed );
            }
            if ( Input.GetKey ( KeyCode.RightArrow ) ) {
                Boule.AddForce ( Vector3.right * speed );
            }
            if ( Input.GetKeyDown ( KeyCode.Space ) ) {
                Boule.AddForce ( Vector3.up * jump );
            }
        }
    }

    void TaskOnClick() {
        click = true;
	}

    void OnTriggerEnter(){
       nb_cube ++;
       //Debug.Log (nb_cube);
       nb.text = nb_cube.ToString();
       if(nb_cube==12) {
        won.enabled = true;
        click = false;
       }
       
    }
    
}
