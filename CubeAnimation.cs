using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CubeAnimation : MonoBehaviour
{   public Rigidbody Cube;
    public bool alive,click;
    public GameObject otherGameObject;

    public float xAngle = 90 , yAngle = 90, zAngle;
    // Start is called before the first frame update

    void Start() {
        Button btn = otherGameObject.GetComponent<Button>();
		btn.onClick.AddListener(TaskOnClick);
        alive = true;
        click = false;
        Cube = GetComponent<Rigidbody>();
   }

    void TaskOnClick(){
        click = true;
	}

    // Update is called once per frame
    void Update() {
        if(alive && click ) {
            Cube.transform.Rotate(1,1,1);
        }        
    }
    
    void OnTriggerEnter(){
        alive = false;
        Destroy(gameObject);
    }

}
