using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NewBehaviourScript : MonoBehaviour
{
   public Button yourButton;
   public Canvas canvas,c;
   public bool click;

	void Start () {
        click = false;
		Canvas c = canvas.GetComponent<Canvas>();
		c.enabled = true;
		Button btn = yourButton.GetComponent<Button>();
		btn.onClick.AddListener(TaskOnClick);
	}

	void TaskOnClick(){
        click = true;
		c.enabled = false ;
	}
    
}
