using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraToBoulette : MonoBehaviour
{
    public GameObject Player;
    public Canvas myCanvas;
    public Vector3 cam_dist;
    public Transform trans;
    // Start is called before the first frame update
    void Start()
    {
       cam_dist = transform.position - Player.transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 cam_pos = Player.transform.position + cam_dist;
        transform.position = cam_pos;
    }
}
